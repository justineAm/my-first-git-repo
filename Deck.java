/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShuffleDeckofCards;

import java.util.Random;

/**
 *
 * @author 2ndyrGroupA
 */
public class Deck {
    final int size = 52;
    Cards[] deckOfCards = new Cards[size];


    public Deck(){
        int count=0;

        String rank[] = {"A ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "10 ", "J ", "Q ", "K "};
        String suit[] = {"Speed", "Clubs", "Diamond", "Heart"};


        for (String s:suit){
            for (String r:rank){

                Cards card = new Cards(s, r);
                this.deckOfCards[count] = card;
                count++;
            }
        }

    }
    
    public void display() {
        
        int count =0;
        for (Cards card : deckOfCards){
            System.out.print(card.rank + card.suit + "\n");
            count++;
            if(count%13==0)
                System.out.println(" ");       
        }

    }
    public void ShuffleCards(){
        Random rand = new Random();
        int j;
        int count =0;
        for(int i=0; i<size; i++){
            j = rand.nextInt(size);
            Cards temporary = deckOfCards[i];
            deckOfCards[i]=deckOfCards[j];
            deckOfCards[j]= temporary;
            
            
        }
        for (Cards card : deckOfCards){
                System.out.print(card.rank + card.suit + "\n");
                count++;
                if(count%13==0)
                    System.out.println("\n");       
            }
    }
    
    
}
